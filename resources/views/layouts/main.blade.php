<!DOCTYPE html>
<html lang="en">

@include('layouts.partials._header')


<body>
    <div class="container">
        @yield('content')
    </div>

@include('layouts.partials._footer')