<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twitter;
use File;

class TwitterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function twitterUserTimeLine(Request $request)
    {
        $data = Twitter::getUserTimeline(['screen_name'=>'tomiqwww','count' => 10, 'format' => 'array']);

        return view('twitter')->withData($data);
    }
}