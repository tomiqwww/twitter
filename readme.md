#Installation

-  Don't forget to `composer update` after pull.

To change which user to parse tweets, open `TwitterController.php`

and change value of `screen_name`;

For example 
```
$data = Twitter::getUserTimeline(['screen_name'=>'tomiqwww','count' => 10, 'format' => 'array']);
```

User's tweets with name `tomiqwww` will be parsed.